# Aiven Exercise

## Introduction
This small program reads project events from an Aiven project and writes them
to a Kafka cluster via a Kafka producer to a Kafka topic. A Kafka consumer then
read the messages and inserts them to a Postgresql table. The producer handles
creation of the topics and the consumer handles creating the postgresql table
needed. 

The producer continuously polls the Aiven api for new events. It maintains the
last timestamp sent to Kafka and any events occurring after this timestamp
are considered new events.

For the purposes of this exercise, upon restarting/starting, the topic
and table are recreated each time.
 
## Requirements
* python3
* pip3
* Aiven account
* kafka cluster running in Aiven
* postgresql database running in Aiven

## Configuration
The configuration for this program uses a simple yaml file. The kafka and 
postgresql configuration lives in this config file. A sample is given
[here](config.valid.test.yml). The config file contains paths to Kafka certs
which are assumed to be downloaded already by the user from Aiven.

## Usage
After cloning/downloading the repo:
```bash
python3 -m venv venv
. venv/bin/activate
pip3 install -r requirements.txt
```

To run both the producer and consumer:
```bash
python3 aiven.py
```

To run the producer by itself
```bash
python3 producer.py
```

To run the consumer by itself
```bash
python3 consumer.py
```

## Tests
Units tests are included to test the configuration loader, producer and consumer independently (requires kafka and postgresql)
* For configuration -- Verifies configuration exists and the schema.
* For producer -- Verifies events are written to kafka.
* For consumer -- Verifies events are written to postgresql.

To run tests:
```bash
python3 -m venv venv
. venv/bin/activate
pytest
```

### References
* https://help.aiven.io/en/collections/341128-kafka
* https://help.aiven.io/en/articles/489572-getting-started-with-aiven-kafka
* https://help.aiven.io/en/collections/341080-postgresql
* https://help.aiven.io/en/articles/489573-getting-started-with-aiven-postgresql
* https://api.aiven.io/doc/#operation/Account.AccountEventList
* https://kafka-python.readthedocs.io/en/master/usage.html
