"""
Starts up aiven producer and consumer
"""

import multiprocessing

import config
import consumer
import producer

if __name__ == '__main__':
    config_yaml = config.load_config()

    aiven_producer = multiprocessing.Process(name="aiven_producer",
                                             target=consumer.run,
                                             args=(config_yaml,))
    aiven_consumer = multiprocessing.Process(name="aiven_consumer",
                                             target=producer.run,
                                             args=(config_yaml,))
    aiven_producer.start()
    aiven_consumer.start()

    aiven_producer.join()
