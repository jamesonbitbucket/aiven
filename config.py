"""
Responsible for loading and validating config file
"""

import logging

import yaml
import yamale

logging.basicConfig(
    format='%(asctime)s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')


def load_config(config_path="config.yml"):
    """
    Loads config from config.yml

    :param config_path:
    :return:
    """
    try:
        with open(config_path, "r") as stream:
            try:
                config_yaml = yaml.safe_load(stream)
            except yaml.YAMLError:
                raise ValueError("Unable to load config file: {}".format(config_path))
    except FileNotFoundError:
        raise ValueError("Unable to find config file: {}".format(config_path))

    try:
        # validates our config file according to config.schema.yml
        schema = yamale.make_schema("config.schema.yml")
        data = yamale.make_data(config_path)
        yamale.validate(schema, data)
    except yamale.YamaleError:
        raise ValueError("Invalid config file: {}".format(config_path))

    return config_yaml
