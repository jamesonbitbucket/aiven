"""
Reads projects events from kafka cluster and writes them to a postgresql table.
"""

import json
import logging
import traceback

import kafka
import psycopg2
import psycopg2.extras
import psycopg2.sql
import retry

import config

DROP_TABLE_STATEMENT = """
    DROP TABLE IF EXISTS {}
"""

CREATE_TABLE_STATEMENT = """
    CREATE TABLE IF NOT EXISTS {}(
        timestamp TIMESTAMP NOT NULL,
        actor TEXT NOT NULL,
        event_desc TEXT,
        event_type TEXT NOT NULL,
        service_name TEXT
    )
"""

INSERT_STATEMENT = \
    "INSERT INTO {} (timestamp, actor, event_desc, event_type, service_name) VALUES %s"


def init(config_yaml):
    """
    Recreates table to write events to if already exists

    :param config_yaml:
    :return:
    """
    postgresql = config_yaml["project"]["postgresql"]
    postgresql_host = postgresql["host"]
    postgresql_port = postgresql["port"]
    postgresql_user = postgresql["user"]
    postgresql_password = postgresql["password"]
    postgresql_database = postgresql["database"]
    postgresql_table = postgresql["table"]

    postgresql_uri = "postgres://{}:{}@{}:{}/{}?sslmode=require".format(postgresql_user,
                                                                        postgresql_password,
                                                                        postgresql_host,
                                                                        postgresql_port,
                                                                        postgresql_database)

    try:
        connection = psycopg2.connect(dsn=postgresql_uri,
                                      cursor_factory=psycopg2.extras.RealDictCursor)
    except psycopg2.Warning:
        traceback.print_exc()
    except psycopg2.Error:
        logging.error("Failed to connect to postgresql")
        raise

    try:
        with connection.cursor() as cursor:
            cursor.execute(
                psycopg2.sql.SQL(DROP_TABLE_STATEMENT).format(
                    psycopg2.sql.Identifier(postgresql_table)))
            cursor.execute(
                psycopg2.sql.SQL(CREATE_TABLE_STATEMENT).format(
                    psycopg2.sql.Identifier(postgresql_table)))
    except psycopg2.Warning:
        traceback.print_exc()
    except psycopg2.Error:
        logging.error("Rolling back drop/create table statements.")
        connection.rollback()
        logging.info("Closing postgresql connection")
        connection.close()
        raise

    try:
        connection.commit()
    except psycopg2.Warning:
        traceback.print_exc()
    except psycopg2.Error:
        logging.error("Failed to commit drop/create statements to postgresql")
        logging.info("Closing postgresql connection")
        connection.close()
        raise

    return connection


def run(config_yaml=config.load_config()):
    """
    Initializes and runs consumer
    :param config_yaml:
    :return:
    """
    logging.info("Starting consumer...")
    connection = init(config_yaml)

    if not connection:
        raise RuntimeError("Postgresql connection not initialized")

    postgresql_table = config_yaml["project"]["postgresql"]["table"]

    consumer = kafka.KafkaConsumer(
        config_yaml["project"]["kafka"]["topics"][0]["name"],
        auto_offset_reset="earliest",
        client_id="james",
        group_id="james-group",
        **config_yaml["project"]["kafka"]["config"],
    )

    try:
        # call poll once to assign partitions first
        poll(consumer)
        while True:
            consume(consumer, connection, postgresql_table)
    except psycopg2.Error:
        logging.info("Closing postgresql connection.")
        connection.close()
        raise
    except kafka.errors.KafkaError:
        logging.info("Closing kafka consumer")
        consumer.close()
        raise


@retry.retry(kafka.errors.KafkaError, tries=3, delay=3)
def poll(consumer, timeout_ms=1000):
    """
    Retrys poll when possible, otherwise reraises exception

    :param:
    :return:
    """
    try:
        messages = consumer.poll(timeout_ms=timeout_ms)
    except kafka.errors.KafkaError as kafka_error:
        if kafka_error.retriable:
            raise

        traceback.print_exc()
        raise RuntimeError("Failed to poll messages from kafka")

    return messages


def consume(consumer, connection, table):
    """
    Pulls messages from kafka and writes to postgresql table

    :param consumer:
    :param connection:
    :param table:
    :return:
    """
    event_tuples = []

    kafka_messages = poll(consumer)

    for messages in kafka_messages.values():
        for message in messages:
            logging.info("Consuming: %s", message.value)
            try:
                event_json = json.loads(message.value)
                event_tuples.append((event_json["time"],
                                     event_json["actor"],
                                     event_json["event_desc"],
                                     event_json["event_type"],
                                     event_json["service_name"]))
            except json.JSONDecodeError:
                traceback.print_exc()
                logging.warning("Failed json decoding %s", message.value)

    try:
        with connection.cursor() as cursor:
            psycopg2.extras.execute_values(cursor,
                                           psycopg2.sql.SQL(INSERT_STATEMENT).
                                           format(psycopg2.sql.Identifier(table)).as_string(
                                               connection),
                                           event_tuples)
    except psycopg2.Warning:
        traceback.print_exc()
    except psycopg2.Error:
        logging.error("Rolling back insert statements.")
        connection.rollback()
        raise

    try:
        connection.commit()
    except psycopg2.Warning:
        traceback.print_exc()
    except psycopg2.Error:
        logging.error("Failed to commit insert statements to postgresql")
        raise

    @retry.retry(kafka.errors.KafkaError, tries=3, delay=3)
    def commit():
        try:
            consumer.commit()
        except kafka.errors.KafkaError as kafka_error:
            if kafka_error.retriable:
                raise

            traceback.print_exc()
            raise RuntimeError("Failed to commit offsets to kafka")

    commit()


if __name__ == "__main__":
    run()
