"""
Gathers project events from aiven api and sends them to a kafka cluster.
"""

import logging
import json
import datetime
import traceback

import requests
import kafka
import dateutil.parser
import retry

import config


def init(config_yaml):
    """
    creates topics to send messages to
    deletes existing topics if exists

    :param config_yaml:
    :return:
    """
    topics = config_yaml["project"]["kafka"]["topics"]
    new_topics = [kafka.admin.NewTopic(**topic) for topic in topics]
    topic_name = topics[0]["name"]

    admin_client = kafka.KafkaAdminClient(**config_yaml["project"]["kafka"]["config"])

    current_topics = admin_client.list_topics()
    logging.info("Current topics %s", admin_client.list_topics())

    # for the purposes of this exercise we just want have a new topic each time it starts up
    if topic_name in current_topics:
        logging.info("Deleting topic %s", topic_name)
        admin_client.delete_topics([topic_name])

    # delete_topics is async
    # we still can't create topics with the same name even when list_topics returns []
    # so we just wait
    while True:
        try:
            logging.info("Adding topics: %s", [topic.name for topic in new_topics])
            admin_client.create_topics(new_topics=new_topics)
        except kafka.errors.TopicAlreadyExistsError:
            logging.info(
                "Waiting for topics %s to be deleted", [topic.name for topic in new_topics])
            continue
        break

    admin_client.close()


def run(config_yaml=config.load_config()):
    """
    Initializes and runs producer
    :param config_yaml:
    :return:
    """

    logging.info("Starting producer...")
    init(config_yaml)

    producer = kafka.KafkaProducer(acks="all", **config_yaml["project"]["kafka"]["config"])

    last_processed_event_timestamp = \
        datetime.datetime.fromtimestamp(0, tz=datetime.timezone.utc)

    try:
        while True:
            last_processed_event_timestamp = produce(config_yaml, producer,
                                                     last_processed_event_timestamp)
    except kafka.errors.KafkaError:
        logging.info("Closing kafka producer")
        producer.close()
        raise


def filter_events(events, last_processed_event_timestamp):
    """
    filters out events before last_processed_event_timestamp

    :param events:
    :param last_processed_event_timestamp:
    :return: filtered events and new last processed timestamp
    """
    current_last_processed_event_timestamp = last_processed_event_timestamp
    filtered_events = []

    for event in events:
        event_timestamp = dateutil.parser.isoparse(event["time"])
        if event_timestamp > current_last_processed_event_timestamp:
            filtered_events.append(event)
            if event_timestamp > last_processed_event_timestamp:
                last_processed_event_timestamp = event_timestamp
                logging.info("Updating last processed event timestamp to %s",
                             last_processed_event_timestamp)

    return filtered_events, last_processed_event_timestamp


def produce(config_yaml, producer, last_processed_event_timestamp):
    """
    retrieves projects events from aiven api and sends to kafka

    :param config_yaml:
    :param producer:
    :param last_processed_event_timestamp:
    :return:
    """
    project_id = config_yaml["project"]["id"]
    api_token = config_yaml["auth"]["api_token"]
    topics = config_yaml["project"]["kafka"]["topics"]

    @retry.retry((requests.exceptions.RequestException, json.decoder.JSONDecodeError),
                 tries=10,
                 delay=3)
    def get_events():
        response = requests.get("https://api.aiven.io/v1/project/{}/events".format(project_id),
                                headers={
                                    "authorization": "aivenv1 {}".format(api_token),
                                    "Content-Type": "application/json"
                                })

        return response.json()

    try:
        events = get_events()["events"]
    except (requests.exceptions.RequestException, json.decoder.JSONDecodeError):
        traceback.print_exc()
        raise ValueError("Unable to retrieve events from aiven.")

    filtered_events, last_processed_event_timestamp = filter_events(events,
                                                                    last_processed_event_timestamp)

    @retry.retry(kafka.errors.KafkaTimeoutError, tries=3, delay=3)
    def send(_event):
        try:
            producer.send(topics[0]["name"], json.dumps(_event).encode("utf-8"))
        except kafka.errors.KafkaError as kafka_error:
            if kafka_error.retriable:
                raise

            traceback.print_exc()
            raise RuntimeError("Failed to queue messages to buffer")

    for event in filtered_events:
        logging.info("Producing: %s", event)
        send(event)

    @retry.retry(kafka.errors.KafkaError, tries=3, delay=3)
    def flush():
        try:
            producer.flush(timeout=15)
        except kafka.errors.KafkaError as kafka_error:
            if kafka_error.retriable:
                raise

            traceback.print_exc()
            raise RuntimeError("Failed to flush producer")

    # force flush queue
    flush()

    return last_processed_event_timestamp


if __name__ == "__main__":
    run()
