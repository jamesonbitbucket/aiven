"""
Unit tests for config.py
"""

import pytest

import config

def test_valid_config():
    """
    :return:
    """
    config.load_config("config.valid.test.yml")


def test_config_not_found():
    """
    :return:
    """
    with pytest.raises(ValueError):
        config.load_config("no_file.yml")


def test_invalid_config():
    """
    :return:
    """
    with pytest.raises(ValueError):
        config.load_config("config.invalid.test.yml")
