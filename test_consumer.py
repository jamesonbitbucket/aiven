"""
Unit tests for consumer.py
"""

import json
import uuid

import pytest
import psycopg2
import psycopg2.sql

import config
import consumer

@pytest.fixture
def postgresql_connection():
    """

    :return:
    """

    def _postgresql_connection(config_yaml):
        postgresql = config_yaml["project"]["postgresql"]
        postgresql_host = postgresql["host"]
        postgresql_port = postgresql["port"]
        postgresql_user = postgresql["user"]
        postgresql_password = postgresql["password"]
        postgresql_database = postgresql["database"]

        postgresql_uri = "postgres://{}:{}@{}:{}/{}?sslmode=require".format(postgresql_user,
                                                                            postgresql_password,
                                                                            postgresql_host,
                                                                            postgresql_port,
                                                                            postgresql_database)
        connection = psycopg2.connect(dsn=postgresql_uri,
                                      cursor_factory=psycopg2.extras.RealDictCursor)
        return connection

    return _postgresql_connection


@pytest.fixture
def postgresql_delete_table(request):
    """

    :param request:
    :return:
    """

    def delete_table():
        cursor = request.node.connection.cursor()
        table = request.node.table
        sql = psycopg2.sql.SQL("DROP TABLE IF EXISTS {}").format(psycopg2.sql.Identifier(table))
        cursor.execute(sql)
        request.node.connection.commit()
        cursor.close()
        request.node.connection.close()

    request.addfinalizer(delete_table)


def test_init(postgresql_connection, postgresql_delete_table, request):
    """

    :param postgresql_connection:
    :param postgresql_delete_table:
    :param request:
    :return:
    """
    # pylint: disable=unused-argument, redefined-outer-name

    config_yaml = config.load_config()
    table_name = str(uuid.uuid4())
    config_yaml["project"]["postgresql"]["table"] = table_name

    connection = postgresql_connection(config_yaml)
    cursor = connection.cursor()

    # for cleanup
    request.node.connection = connection
    request.node.table = table_name

    sql = psycopg2.sql.SQL(
        "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'")

    cursor.execute(sql)

    tables = [row["table_name"] for row in cursor.fetchall()]
    assert table_name not in tables

    consumer.init(config_yaml)

    cursor.execute(sql)
    tables = [row["table_name"] for row in cursor.fetchall()]
    assert table_name in tables


def test_consume(monkeypatch, postgresql_connection, postgresql_delete_table, request):
    """

    :param monkeypatch:
    :param postgresql_connection:
    :param postgresql_delete_table:
    :param request:
    :return:
    """
    # pylint: disable=unused-argument, redefined-outer-name
    config_yaml = config.load_config()
    table_name = str(uuid.uuid4())
    config_yaml["project"]["postgresql"]["table"] = table_name

    class MockConsumerRecord:
        """
        Mocks kafka's ConsumerRecord
        """
        # pylint: disable=too-few-public-methods
        def __init__(self, **kwargs):
            self.value = kwargs.get("value", {})

    topics = [
        {
            "actor": "email@domain.com",
            "event_desc": "Created Kafka topic 'test2'",
            "event_type": "service_update",
            "service_name": "james-kafka",
            "time": "2020-07-06T21:27:33Z",
        },
        {
            "actor": "Aiven Operations",
            "event_desc": "Created 'kafka' service 'kafka-307b49a1' "
                          "with plan 'startup-2' in cloud 'do-sfo'",
            "event_type": "service_create",
            "service_name": "kafka-307b49a1",
            "time": "2020-07-03T18:28:08Z",
        }
    ]

    messages = {
        "topics": (MockConsumerRecord(value=json.dumps(topic)) for topic in topics)
    }

    class MockConsumer:
        """
        Mocks Kafka's consumer client
        """
        @staticmethod
        def poll(*_, **__):
            """
            Mocks poll method
            :param _:
            :param __:
            :return:
            """
            return messages

        @staticmethod
        def commit():
            """
            Mocks commit method
            :return:
            """

    connection = postgresql_connection(config_yaml)

    consumer.init(config_yaml)
    consumer.consume(MockConsumer(), connection, table_name)

    cursor = connection.cursor()

    # for cleanup
    request.node.connection = connection
    request.node.table = table_name

    sql = psycopg2.sql.SQL("SELECT COUNT(*) FROM {}").format(psycopg2.sql.Identifier(table_name))

    cursor.execute(sql)
    result = cursor.fetchone()
    assert result["count"] == len(topics)
