"""
Unit tests for producer.py
"""

import datetime
import json
import logging
import uuid

import dateutil
import requests
import kafka
import pytest

import config
import producer


def test_filter_events():
    """
    :return:
    """
    events = [
        {"time": "2020-07-05T00:00:00Z"},
        {"time": "2020-07-07T00:00:00Z"}
    ]
    last_processed_event_timestamp = dateutil.parser.isoparse("2020-07-06T00:00:00Z")
    assert (events[-1:], dateutil.parser.isoparse(events[-1]["time"])) == \
           producer.filter_events(events, last_processed_event_timestamp)


@pytest.fixture
def kafka_admin():
    """
    :return:
    """

    def _kafka_admin(config_yaml):
        client = kafka.KafkaAdminClient(**config_yaml["project"]["kafka"]["config"])
        return client

    return _kafka_admin


@pytest.fixture
def kafka_producer():
    """
    :return:
    """

    def _kafka_producer(config_yaml):
        client = kafka.KafkaProducer(acks="all", **config_yaml["project"]["kafka"]["config"])
        return client

    return _kafka_producer


@pytest.fixture
def kafka_consumer():
    """

    :return:
    """

    def _kafka_consumer(config_yaml):
        topics = config_yaml["project"]["kafka"]["topics"]
        client = kafka.KafkaConsumer(topics[0]["name"],
                                     auto_offset_reset="earliest",
                                     client_id=str(uuid.uuid4()),
                                     group_id=str(uuid.uuid4()),
                                     **config_yaml["project"]["kafka"]["config"])
        return client

    return _kafka_consumer


@pytest.fixture
def kafka_delete_topic(request):
    """

    :param request:
    :return:
    """

    def delete_topic():
        request.node.kafka_admin_client.delete_topics([request.node.topic_name])

    request.addfinalizer(delete_topic)


@pytest.fixture
def kafka_close_producer(request):
    """

    :param request:
    :return:
    """

    def close_producer():
        request.node.kafka_producer.close(timeout=60)

    request.addfinalizer(close_producer)


def test_init(kafka_admin, kafka_delete_topic, request):
    """

    :param kafka_admin:
    :param kafka_delete_topic:
    :param request:
    :return:
    """
    # pylint: disable=unused-argument, redefined-outer-name

    config_yaml = config.load_config()
    topic_name = str(uuid.uuid4())
    config_yaml["project"]["kafka"]["topics"][0]["name"] = topic_name

    kafka_admin = kafka_admin(config_yaml)

    # for cleanup post test
    request.node.kafka_admin_client = kafka_admin
    request.node.topic_name = topic_name

    current_topics = kafka_admin.list_topics()
    assert topic_name not in current_topics

    producer.init(config_yaml)

    current_topics = kafka_admin.list_topics()
    assert topic_name in current_topics


def test_produce(monkeypatch, kafka_admin, kafka_producer, kafka_consumer, kafka_close_producer,
                 request):
    """
    Currently a bit flaky.

    :param monkeypatch:
    :param kafka_admin:
    :param kafka_producer:
    :param kafka_consumer:
    :param kafka_close_producer:
    :param request:
    :return:
    """
    # pylint: disable=unused-argument, redefined-outer-name, too-many-arguments, too-many-locals

    expected_events = {
        "events": [
            {
                "actor": "email@domain.com",
                "event_desc": "Created Kafka topic 'test1'",
                "event_type": "service_update",
                "service_name": "james-kafka",
                "time": "2020-07-06T21:27:33Z",
            }
        ]
    }

    class MockResponse:
        """
        Mocks requests module
        """
        # pylint: disable=too-few-public-methods

        @staticmethod
        def json():
            """
            :return: static list of events
            """
            return expected_events

    def mock_get(*_, **__):
        """
        Mocks get method
        :param _:
        :param __:
        :return:
        """
        return MockResponse()

    monkeypatch.setattr(requests, "get", mock_get)
    config_yaml = config.load_config()

    # topic just for this test
    topic_name = str(uuid.uuid4())
    config_yaml["project"]["kafka"]["topics"][0]["name"] = topic_name

    kafka_admin = kafka_admin(config_yaml)
    new_topics = [
        kafka.admin.NewTopic(
            name=topic_name,
            num_partitions=1,
            replication_factor=1,
        )
    ]

    kafka_admin.create_topics(new_topics=new_topics)

    kafka_producer = kafka_producer(config_yaml)

    # for cleanup post test
    request.node.kafka_producer = kafka_producer

    last_processed_event_timestamp = datetime.datetime.fromtimestamp(0, tz=datetime.timezone.utc)
    producer.produce(config_yaml, kafka_producer, last_processed_event_timestamp)
    consumer = kafka_consumer(config_yaml)

    actual_messages = []
    consumer.poll(timeout_ms=1000)

    consumer.seek_to_beginning()

    for messages in consumer.poll(timeout_ms=1000 * 30).values():
        for message in messages:
            logging.info("Consuming %s", message.value)
            actual_messages.append(json.loads(message.value))

    consumer.commit()

    if actual_messages == expected_events["events"]:
        kafka_admin.delete_topics([topic_name])

    assert actual_messages == expected_events["events"]
